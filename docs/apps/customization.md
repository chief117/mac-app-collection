# Customization

## Desktop
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[GeekTool](https://www.tynsoe.org/v2/geektool/)| Customize your desktop with great flexibility. There are four modules available that you can use for different types of informations.|[Website](https://www.tynsoe.org/v2/geektool/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Dock
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[VerticalBar](https://github.com/DeromirNeves/VerticalBar)| Add a vertical or horizontal bar with space to the Dock similar to the default one.|[Github](https://github.com/DeromirNeves/VerticalBar)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Wallpapers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Irvue](https://irvue.tumblr.com/)| Gorgeous high-resolution wallpapers on your Mac from Unsplash with new ones every day. |[AppStore](https://apps.apple.com/app/irvue-unsplash-wallpapers/id1039633667)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Plash](https://github.com/sindresorhus/Plash)| Make any website your Mac desktop wallpaper.|[GitHub](https://github.com/sindresorhus/Plash)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
