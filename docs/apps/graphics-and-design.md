# Graphics and Design

## Bitmap
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Acorn](https://secure.flyingmeat.com/acorn/)| Full-featured photo editor for MacOS.|[AppStore](https://secure.flyingmeat.com/acorn/appstore/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Affinity Photo](https://affinity.serif.com/en-us/photo/)| Professional Imagine Editing Software |[AppStore](https://apps.apple.com/us/app/affinity-photo/id824183456?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pixelmator Pro](https://www.pixelmator.com/pro/)| Professional image editing tools that anyone can use.|[AppStore](https://apps.apple.com/us/app/pixelmator-pro/id1289583905?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Color pickers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Color Picker Plus](https://github.com/viktorstrate/color-picker-plus)| An extension to the macOS Colors panel, that is inspired by the color picker from Adobe Photoshop.|[Github](https://github.com/viktorstrate/color-picker-plus#color-picker-plus)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[ColorSlurp](http://colorslurp.com/)| ColorSlurp is the ultimate color productivity booster for designers and developers.|[AppStore](https://apps.apple.com/us/app/colorslurp/id1287239339?ls=1&mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Font manager
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[RightFont](https://rightfontapp.com/)| Professional font manager app for Mac.|[Website](https://rightfontapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Typeface](https://typefaceapp.com/)| Your all new favorite font app.|[AppStore](https://geo.itunes.apple.com/us/app/typeface/id1062679359?mt=12&app=apps)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Gif
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[GifCapture](https://github.com/onmyway133/GifCapture)| Gif capture app for macOS|[Github](https://github.com/onmyway133/GifCapture)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Gifski](https://github.com/sindresorhus/Gifski)| Convert videos to high-quality GIFs on your Mac |[Github](https://github.com/sindresorhus/Gifski)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Gifox](https://gifox.io/)| Delightful GIF Recording and Sharing App|[AppStore](https://apps.apple.com/us/app/gifox-2/id1461845568?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Icons
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[IconJar](https://geticonjar.com/)| Icon manager for MacOS|[Website](https://geticonjar.com/)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iConvert Icons](https://iconverticons.com/)| Easiest way to create and convert custom icons|[AppStore](https://apps.apple.com/us/app/iconvert-icons/id515197296?app=apps&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Iconizer](http://raphaelhanneken.github.io/iconizer/)| Create Xcode asset catalogs on the fly.|[Website](http://raphaelhanneken.github.io/iconizer/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Publishing
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Affinity Publisher](https://affinity.serif.com/en-us/publisher/)| Professional desktop publishing software |[AppStore](https://apps.apple.com/us/app/affinity-publisher/id881418622?mt=12)|![Paid](symbols/freemium.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Swift Publisher](https://www.swiftpublisher.com/)| All-purpose page layout and desktop publisher for Mac. |[AppStore](https://apps.apple.com/us/app/swift-publisher-5/id1058362543?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[ImageOptim](https://imageoptim.com/mac)| Removes bloated metadata. Saves disk space & bandwidth by compressing images without losing quality.|[Website](https://imageoptim.com/mac)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[PixelSnap](https://getpixelsnap.com/)| The fastest tool for measuring anything on your screen|[Website](https://getpixelsnap.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PhotoBulk](https://photobulkeditor.com/)| Easy-to-use bulk image editor that lets you add multiple text/image watermarks, resize, optimize, rename and convert hundreds and thousands of images in just one click. |[AppStore](https://apps.apple.com/us/app/photobulk-watermark-resize/id537211143?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Screenbar](https://github.com/crilleengvall/Screenbar)| A macOS menubar app for automating screenshots.|[Github](https://github.com/crilleengvall/Screenbar)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Vector
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Affinity Designer](https://affinity.serif.com/en-us/designer/)| Professional Graphic Design software|[AppStore](https://apps.apple.com/app/affinity-designer/id824171161?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Sketch](https://www.sketch.com/)| Vector graphics editor for macOS made for user interface and user experience design.|[Website](https://www.sketch.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Logoist](https://www.syniumsoftware.com/logoist)| Powerful Vector Design App|[AppStore](https://apps.apple.com/app/logoist-4/id1493005285?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
