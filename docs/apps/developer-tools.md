# Developer tools

## Apple
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[WWDC for macOS](https://wwdc.io/)| The unofficial WWDC app for macOS.|[Website](https://wwdc.io/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[NativeConnect](https://nativeconnect.app/)| Native Desktop Client for App Store Connect.|[Website](https://nativeconnect.app/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## CSV
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[TableTool](https://github.com/jakob/TableTool)| A simple CSV editor for the Mac.|[Github](https://github.com/jakob/TableTool)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Easy CSV Editor](https://apps.apple.com/app/easy-csv-editor/id1171346381?mt=12)| Easy CSV Editor is one of the best editors for CSV and TSV data files. In a clean and user-friendly interface, it offers simplicity and flexibility to edit both small and large documents.|[AppStore](https://apps.apple.com/app/easy-csv-editor/id1171346381?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Database manager
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[SequelPro](http://www.sequelpro.com/)| Sequel Pro is a fast, easy-to-use Mac database management application for working with MySQL databases.|[Website](http://www.sequelpro.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[TablePlus](https://tableplus.com/)| Modern, native, and friendly GUI tool for relational databases: MySQL, PostgreSQL, SQLite & more. |[Website](https://tableplus.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Base](http://menial.co.uk/base/)| Base is an application for creating, designing, editing and browsing SQLite 3 database files.|[Website](http://menial.co.uk/base/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Postioco](https://eggerapps.at/postico/)| A Modern PostgreSQL Client for the Mac.|[Website](https://eggerapps.at/postico/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PSequel](http://www.psequel.com/)| PSequel provides a clean and simple interface for you to perform common PostgreSQL tasks quickly.|[Website](http://www.psequel.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Core Data Lab](https://betamagic.nl/products/coredatalab.html)| View, analyze and track your Core Data app’s data (SQLite)|[AppStore](https://apps.apple.com/app/core-data-lab/id1460684638?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Design tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Sketch to Xcode](https://sketchtoxcode.com/)| Drop in your Sketch file to export as Color Assets and Swift code.|[AppStore](https://apps.apple.com/app/id1483874460?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Development Servers 
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[MAMP Pro](https://www.mamp.info/en/mamp-pro/mac/)| Easy to use webdev stack for local development. (Apache, Nginx, PHP & MySQL)|[Website](https://www.mamp.info/en/mamp-pro/mac/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[RedisApp](https://github.com/jpadilla/redisapp)| Easy way to manage Redis on the Mac via the menubar.|[Github](https://github.com/jpadilla/redisapp)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Dictionaries
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Dash](https://kapeli.com/dash)| Instant offline access to 200+ API documentation sets.|[Website](https://kapeli.com/dash)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Diff/Merge
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Kaleidoscope](https://www.kaleidoscopeapp.com/)| Kaleidoscope is the world’s most powerful file comparison app.|[AppStore](https://apps.apple.com/us/app/kaleidoscope/id587512244?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Documentation
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Mandrake](https://sveinbjorn.org/mandrake)| Native man page editor for macOS with syntax highlighting, live mandoc syntax validation and a live-updating rendered preview.|[Website](https://sveinbjorn.org/mandrake)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CHM to PDF Converter Plus](https://apps.apple.com/se/app/chm-to-pdf-converter-plus/id1498867553?mt=12)| Easy tool for batch conversion of CHM(Compiled HTML Help) files into PDF files.|[AppStore](https://apps.apple.com/se/app/chm-to-pdf-converter-plus/id1498867553?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CHM File Reader](https://apps.apple.com/se/app/chm-file-reader/id1498728464?mt=12)| Simple app open and read CHM files, and convert CHM document to PDF format.|[AppStore](https://apps.apple.com/se/app/chm-file-reader/id1498728464?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## IDEs
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Xcode](https://developer.apple.com/xcode/)| Apple's IDE for macOS containing a suite of software development tools for macOS, iOS, iPadOS, watchOS, and tvOS.|[AppStore](https://apps.apple.com/us/app/xcode/id497799835?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CodeRunner](https://coderunnerapp.com/)| A lightweight, multi-language programming editor for macOS.|[AppStore](https://apps.apple.com/us/app/coderunner-3/id955297617?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[VisualStudio for Mac](https://visualstudio.microsoft.com/vs/mac/)| Develop apps and games for iOS, Android, and web using .NET. |[Website](https://visualstudio.microsoft.com/vs/mac/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Packaging
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[DropDMG](https://apps.apple.com/se/app/dropdmg/id414234859?mt=12)| DropDMG is the easiest way to create macOS disk images, as well as cross-platform archives.|[AppStore](https://apps.apple.com/se/app/dropdmg/id414234859?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[create-dmg](https://github.com/sindresorhus/create-dmg)| Create a good-looking DMG for your macOS app in seconds.|[Github](https://github.com/sindresorhus/create-dmg)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## RegEx
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Patterns](https://krillapps.com/patterns/)| The ultimate tool for working with regular expressions.|[AppStore](http://itunes.apple.com/us/app/patterns-the-regex-app/id429449079?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Expressions](https://www.apptorium.com/expressions)| An app to play with regular expressions. Easily and nicely. |[AppStore](https://apps.apple.com/us/app/expressions/id913158085?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Text editors
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[CotEditor](https://coteditor.com/)| The Plain-Text Editor for macOS. |[AppStore](https://itunes.apple.com/app/coteditor/id1024640650?ls=1)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SublimeText](http://www.sublimetext.com/)| A fast and sophisticated text editor for code, markup and prose.|[Website](http://www.sublimetext.com/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[TextMate](https://macromates.com/)| Powerful and customizable text editor with support for a huge list of programming languages.|[Website](https://macromates.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[VimR](https://github.com/qvacua/vimr)| Neovim GUI for macOS.|[Github](https://github.com/qvacua/vimr)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Visual Studio Code](https://code.visualstudio.com/)| Free, multi-platform and open-source code editor by Microsoft **☢ ELECTRON ☢**. |[Website](https://code.visualstudio.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[BBEdit](www.barebones.com/products/bbedit/)| Simple professional text, code, and markup editor for the Macintosh. |[AppStore](https://apps.apple.com/app/bbedit/id404009241?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Tools & Automation
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[TeaCode](https://www.apptorium.com/teacode)| Don't repeat yourself. Write your code snippets dynamicly.|[Website](https://www.apptorium.com/teacode)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Translation
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[POEDIT](https://poedit.net/)| The fastest and most convenient way to translate apps & sites with gettext.|[Website](https://poedit.net/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iOSLocalizationEditor](https://github.com/igorkulman/iOSLocalizationEditor)| Simple macOS editor app to help you manage iOS and macOS app localizations by allowing you to edit all the translations side by side.|[Github](https://github.com/igorkulman/iOSLocalizationEditor)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Web builder
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Expresso](https://www.espressoapp.com/)| Espresso helps you write, code, design, build and publish with flair and efficiency.|[Website](https://www.espressoapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|



## VCS (Version control systems)

### Git
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Tower](https://www.git-tower.com/mac)| Most powerful Git-client for MacOS.|[Website](https://www.git-tower.com/mac)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SourceTree](https://www.sourcetreeapp.com/)| Free ans simple Git client.|[Website](https://www.sourcetreeapp.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Fork](https://git-fork.com/)| Fast and friendly git client for Mac and Windows.|[Website](https://git-fork.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[GitUp](https://gitup.co/)| Visualize your repo like a pro.|[Website](https://gitup.co/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[GitFox](https://www.gitfox.app/)| Commit faster, improve your code quality with superior diffs - and look good doing it.|[AppStore](https://apps.apple.com/at/app/gitfox/id1475511261?l=en&mt=12)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|

### SVN
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Versions](https://versionsapp.com/)| Native subversion client for Mac.|[Website](https://versionsapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Xversion](https://www.ikoder.com/)| Super easy enterprise class version control. Packed with features, blazing fast and beautifully designed.|[AppStore](https://apps.apple.com/app/xversion/id1105845111?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SnailSVN](https://langui.net/snailsvn/)| SnailSVN is a TortoiseSVN-like Apache Subversion (SVN) client, implemented as a Finder extension.|[AppStore](https://apps.apple.com/app/snailsvn-lite-svn-for-finder/id1063090543?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Cornerstone](https://cornerstone.assembla.com/)| The ultimate Subversion client for Mac just got better.|[Website](https://cornerstone.assembla.com/)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|

## Other
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[App Language Chooser](https://apps.apple.com/us/app/app-language-chooser/id451732904?mt=12)| App Language Chooser lets you run an application using the language of your choice, to display the user interface.|[AppStore](https://apps.apple.com/us/app/app-language-chooser/id451732904?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SnippetsLab](https://www.renfei.org/snippets-lab/)| A full-featured, professional code snippets manager. |[AppStore](https://itunes.apple.com/us/app/snippetslab/id1006087419)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Sitesucker](https://ricks-apps.com/osx/sitesucker/)| SiteSucker is a Macintosh application that automatically downloads websites from the Internet.|[AppStore](https://itunes.apple.com/us/app/sitesucker/id442168834?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Hopper](https://www.hopperapp.com/)| The macOS and Linux Disassembler.|[Website](https://www.hopperapp.com/)|![Paid](symbols/freemium.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[xScope](https://xscopeapp.com/)| Created specifically for designers & developers, xScope is a powerful set of tools that are ideal for measuring, inspecting & testing on-screen graphics and layouts.|[AppStore](https://apps.apple.com/us/app/xscope-4/id889428659?ign-mpt=uo%3D4&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Carbinize](https://www.dangercove.com/carbonize/)| Generate gorgeous, highly customizable images from your code snippets. Ready to save or share.|[AppStore](https://apps.apple.com/app/carbonize/id1451177988)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Lantern](https://icing.space/tools/lantern/)| An open source Mac app for website auditing and crawling.|[AppStore](https://itunes.apple.com/us/app/lantern-website-crawler-for/id991526452?ls=1&mt=12)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[stts](https://github.com/inket/stts)| A simple macOS app for monitoring the status of cloud services.|[Github](https://github.com/inket/stts)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[OpenInTerminal](https://github.com/Ji4n1ng/OpenInTerminal)| Finder Toolbar app for macOS to open the current directory in Terminal, iTerm, Hyper or Alacritty|[Github](https://github.com/Ji4n1ng/OpenInTerminal)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Touch Bar Preview](https://touchbar.github.io/)| A small open source app to display your designs on the Touch Bar of MacBook Pro.|[Website](https://touchbar.github.io/)|![Freem](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Paw](https://paw.cloud/)| Paw is a full-featured HTTP client that lets you test and describe the APIs you build or consume.|[Website](https://paw.cloud/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|

