# Internet

## BitTorrent
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Transmission](https://transmissionbt.com/)| Fast, Easy and Free BitTorrent client|[Website](https://transmissionbt.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## File transfer
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Transmit](https://panic.com/transmit/)| Upload, download, and manage files on tons of servers with an easy, familiar, and powerful UI.|[AppStore](https://apps.apple.com/us/app/transmit-5/id1436522307?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Cyberduck](https://cyberduck.io/)| Libre server and cloud storage browser with support for FTP, SFTP, WebDAV, Amazon S3, OpenStack Swift, Backblaze B2, Microsoft Azure & OneDrive, Google Drive and Dropbox. |[AppStore](https://apps.apple.com/us/app/cyberduck/id409222199?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[FileZilla](https://filezilla-project.org/)| Your classic FTP client for Mac |[Website](https://filezilla-project.org/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Mountain Duck](https://mountainduck.io)| Mount server and cloud storage as a disk in Finder on macOS |[AppStore](https://apps.apple.com/us/app/mountain-duck/id1024974133?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[ViperFTP](https://viperftp.com/)| Simple, user-friendly yet powerful FTP client for Mac|[AppStore](https://apps.apple.com/us/app/viper-ftp/id995745423)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Network Monitoring
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Up & Down](https://github.com/gjiazhe/Up-Down)| A menu bar widget that monitors upload and download speeds|[Github](https://github.com/gjiazhe/Up-Down)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[StatusBuddy](https://gumroad.com/l/statusbuddy)| Keep track of Apple's developer and consumer system statuses right in your menu bar.|[Website](https://gumroad.com/l/statusbuddy)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Network Radar](https://witt-software.com/networkradar/)| Scan and monitor your network. |[AppStore](https://apps.apple.com/app/id507659816?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Net Radar](https://betamagic.nl/products/netradar.html)| The first VPN monitor for Mac that double-checks the status of your VPN connection.|[AppStore](https://apps.apple.com/se/app/net-radar/id1036406076?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Networking Tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Proxyman](https://proxyman.io/)| Proxyman is a native, high-performance macOS application, which enables developers to observe and manipulate HTTP/HTTPS requests.|[Website](https://proxyman.io/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[TripMode](https://www.tripmode.ch/)| Easily stop background apps and updates from draining your Internet data and speed when you most need them, e.g. on hotspots or public Wi-Fi.	|[Website](https://www.tripmode.ch/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Loading](http://bonzaiapps.com/en/loading/)| Simple network activity monitor for MacOS|[Website](http://bonzaiapps.com/en/loading/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Wifi Explorer](https://www.adriangranados.com/)| Scan, monitor and troubleshoot wireless networks using your Mac's built-in Wi-Fi adapter.|[AppStore](https://apps.apple.com/us/app/wifi-explorer/id494803304?mt=12&ign-mpt=uo%3D4)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Surge](https://nssurge.com/)| Advanced Network Toolbox for Mac & iOS|[Website](https://nssurge.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Remote Control/Management
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[TeamViewer](https://www.teamviewer.com/en-us/)| The All-in-One Solution for Secure Remote Access and Support|[Website](https://www.teamviewer.com/en-us/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Screens](https://edovia.com/en/screens-mac/)| Control any computer from your Mac from anywhere in the world.|[Website](https://edovia.com/en/screens-mac/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[AnyDesk](https://anydesk.com/en)| Remote Desktop Software. AnyWhere. AnyTime. AnyDesk|[Website](https://anydesk.com/en)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Royal TS](https://www.royalapps.com/ts/mac/features)| Premium Comprehensive Remote Management Solution|[Website](https://www.royalapps.com/ts/mac/features)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## SSH
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[CoreTunnel](https://coretunnel.app/)| The missing tunnel manager, compatible with OpenSSH, automatic and intuitive. Don't waste your time; be productive.|[AppStore](https://apps.apple.com/us/app/core-tunnel/id1354318707?ls=1&mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## VPN
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Tunnelblick](https://tunnelblick.net/)| Free software for OpenVPN on macOS|[Website](https://tunnelblick.net/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Viscosity](https://www.sparklabs.com/viscosity/)| A first class OpenVPN client that lets you secure your network with ease & style.|[Website](https://www.sparklabs.com/viscosity/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[WireGuard](https://www.wireguard.com/)| Fast, modern, and secure VPN tunnel. This app allows users to manage and use WireGuard tunnels.|[AppStore](https://apps.apple.com/us/app/wireguard/id1451685025?mt=12)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Other
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Integrity](https://peacockmedia.software/mac/integrity/free.html)| Free website link checker for Mac|[Website](https://peacockmedia.software/mac/integrity/free.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Netler](https://www.imagetasks.com/netler-network-monitor/)| Network monitor for Notification Center and Menu Bar|[AppStore](https://apps.apple.com/us/app/netler/id1401495844?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
