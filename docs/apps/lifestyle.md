# Lifestyle

## News readers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[News Explorer](https://betamagic.nl/products/newsexplorer.html)| News reader with read-later with sync support for your RSS, JSON, Atom and Twitter feed subscriptions.|[AppStore](https://apps.apple.com/app/news-explorer/id1032670789?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[NetNewsWire](https://ranchero.com/netnewswire/)| NetNewsWire is a free and open source RSS reader for Mac and iOS.|[Website](https://ranchero.com/netnewswire/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[ReadKit](https://readkitapp.com/)| Have all your Instapaper, Pocket, Pinboard, Feedly, Fever, NewsBlur, Feed Wrangler, Feedbin and RSS feeds in one convenient place.|[AppStore](https://itunes.apple.com/app/readkit/id588726889?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Reeder](https://reederapp.com/)| High quality news reader for Mac and iOS with support for various sync services.|[AppStore](https://apps.apple.com/us/app/reeder-4/id1449412482?ls=1&mt=12)|![Paid](symbols/Paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|

## Photos
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Pixave](http://www.littlehj.com/mac/)| Smartest way to organize your photos and images|[AppStore](https://apps.apple.com/app/pixave/id924891282?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Weather
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Weather guru](https://fiplab.com/apps/weather-guru-for-mac)| Accurate Weather Forecasts - Hourly Data for 7 Days.|[AppStore](https://apps.apple.com/app/weather-guru-hourly-forecasts/id1052302422?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Writing
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Day One](https://dayoneapp.com/)| Journaling about your life a simple pleasures.|[AppStore](https://apps.apple.com/app/day-one/id1055511498)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
