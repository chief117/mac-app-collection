# Mac App Collection

Find hidden gems to improve your Mac.


## Why this list?

The software eco-system for macOS over the last decade has been in a huge growing state. There's a lot of new software and apps which offers high quality, native and polished experience, or just enhancing the existing macOS experience to suit personal needs.

Due to the growth like many other store fronts, sooner or later it gets hard to browse for hidden gems and new upcoming software. I made this list as a personal goto when others ask me for good software recommendations. This site has no commercial side other other than pointing users to support the developers, trough the Mac App Store when available.

## Help me improve the list

This list is by no mean perfect. If you want to add a software to the list or change things up, please give me feedback trough the links in the top right coner or suggest a change on the specific category page. Check out the webstie for more information about curation of the software.

## License

This project is licensed under MIT
